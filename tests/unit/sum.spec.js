function sum (a,b) {
    return a + b;
}

describe("sum()",() =>{
    it('should return correct sum',  () =>{
        expect(sum(5,6)).toBe(11);
    });
})
